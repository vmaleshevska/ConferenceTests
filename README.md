Prerequisites:
Latest python2.7 version.
Latest PyCharm version.

Installing requirements:
pip install -r requirements.txt


Running the tests:
1.Clone the repository
git clone https://gitlab.com/vmaleshevska/ConferenceTests.git
2.Open the project in PyCharm
3.Set the default test runner to pytest
4.Run the tests