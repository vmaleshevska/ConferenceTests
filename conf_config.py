import os

PROJECT_ROOT_PATH = os.path.abspath(os.path.dirname(__file__))
CHROMEDRIVER_PATH = os.path.join(PROJECT_ROOT_PATH, "dependencies", "chromedriver")
SCREENSHOT_PATH = os.path.join(PROJECT_ROOT_PATH, "screenshots", "{}")

CONF_URL = "https://app.staging.realurl"