from page_objects.locators import LoginPageLocator


class JoinMeetingPage(object):

    def __init__(self, driver):
        self.driver = driver

    def join_meeting(self, name, email):
        self.driver.find_element(*LoginPageLocator.LOGIN_NAME_INPUT).send_keys(name)
        self.driver.find_element(*LoginPageLocator.LOGIN_EMAIL_INPUT).send_keys(email)
        self.click_join_meeting()

    def click_join_meeting(self):
        self.driver.find_element(*LoginPageLocator.LOGIN_JOIN_BUTTON).click()

    def is_displayed_join_meeting(self):
        return self.driver.find_element(*LoginPageLocator.LOGIN_JOIN_BUTTON).is_displayed()

    def email_validation_text(self):
        return self.driver.find_element(
            *LoginPageLocator.LOGIN_EMAIL_VALIDATION_MESSAGE).is_displayed()