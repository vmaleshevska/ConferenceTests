from time import sleep

from selenium.webdriver.common.keys import Keys
from helpers import wait_for_element_to_be_clickable
from page_objects.locators import InConfMenuLocators


class InMeetingPage(object):

        def __init__(self, driver):
            self.driver = driver

        def return_menu_audio_button_state(self):
            return self.driver.find_element(*InConfMenuLocators.AUDIO_BUTTON)

        def click_menu_chat_button_state(self):
            #wait_for_element_to_be_clickable(self, *InConfMenuLocators.CHAT_BUTTON, timeout=22)
            sleep(1)
            self.driver.find_element(*InConfMenuLocators.CHAT_BUTTON).click()

        def send_chat_message(self, message):
            self.click_menu_chat_button_state()
            self.driver.find_element(*InConfMenuLocators.CHAT_TYPE_FORM).send_keys(message)
            self.driver.find_element(*InConfMenuLocators.CHAT_TYPE_FORM).send_keys(Keys.ENTER)

        def return_chat_text(self):
            return self.driver.find_element(*InConfMenuLocators.CHAT_TEXT_FORM).text

        def click_end_meeting_button(self):
            wait_for_element_to_be_clickable(self, *InConfMenuLocators.END_MEETING_BUTTON)
            self.driver.find_element(*InConfMenuLocators.END_MEETING_BUTTON).click()
