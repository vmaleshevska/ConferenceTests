from helpers import wait_for_element_to_be_clickable
from page_objects.locators import LeaveMeetingPageLocators


class LeaveMeetingPage(object):

        def __init__(self, driver):
            self.driver = driver

        def click_leave_button(self):
            wait_for_element_to_be_clickable(self, *LeaveMeetingPageLocators.LEAVE_BUTTON)
            self.driver.find_element(*LeaveMeetingPageLocators.LEAVE_BUTTON).click()
