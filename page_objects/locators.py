from selenium.webdriver.common.by import By


class LoginPageLocator(object):
    LOGIN_NAME_INPUT = (By.ID, "name")
    LOGIN_EMAIL_INPUT = (By.ID, "email")
    LOGIN_JOIN_BUTTON = (By.NAME, "button")
    LOGIN_EMAIL_VALIDATION_MESSAGE = (By.XPATH, "//*[contains(., 'The email field must be a valid email.')]")


class SelectAudioOptionDialogLocators(object):
    xpath = "//button[@data-audio-option='{}']"
    AUDIO_BUTTON = (By.XPATH, xpath.format('computer'))
    DIAL_IN_BUTTON = (By.XPATH, xpath.format('phone'))
    SCREEN_SHARE_BUTTON = (By.XPATH, xpath.format('view-only'))


class InConfDialogLocators(object):
    xpath = "//span[@class='_2bo7L' and contains(text(), '{}')]/parent::button"
    MUTE_BUTTON = (By.XPATH, xpath.format('Mute'))
    HANG_UP_BUTTON = (By.XPATH, xpath.format('Hang up'))


class InConfMenuLocators(object):
    AUDIO_BUTTON = (By.XPATH, "//i[@class='j-icon-speaker']/parent::button")
    CHAT_BUTTON = (By.XPATH, "//i[@class='j-icon-chat']/parent::button")
    END_MEETING_BUTTON = (By.XPATH, "//i[@class='j-icon-exit-2']/parent::button")
    CHAT_TYPE_FORM = (By.CLASS_NAME, "emojionearea-editor")
    CHAT_TEXT_FORM = (By.CLASS_NAME, "nygZw")


class LeaveMeetingPageLocators(object):
    LEAVE_BUTTON = (By.XPATH, "//a[contains(text(), 'Leave')]")