from helpers import wait_for_element_to_be_clickable
from page_objects.locators import SelectAudioOptionDialogLocators


class ConferencePage(object):

    def __init__(self, driver):
        self.driver = driver

    def select_audio_option(self, audio=None, dial_in=None, share_screen=None):
        wait_for_element_to_be_clickable(self, *SelectAudioOptionDialogLocators.AUDIO_BUTTON)
        if audio:
            self.driver.find_element(*SelectAudioOptionDialogLocators.AUDIO_BUTTON).click()
        if dial_in:
            self.driver.find_element(*SelectAudioOptionDialogLocators.DIAL_IN_BUTTON).click()
        if share_screen:
            self.driver.find_element(*SelectAudioOptionDialogLocators.SCREEN_SHARE_BUTTON).click()



