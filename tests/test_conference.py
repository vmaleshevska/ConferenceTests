import uuid

from selenium.webdriver.chrome import webdriver

from conf_config import CHROMEDRIVER_PATH, CONF_URL
from helpers import save_screenshot
from page_objects.screen_conference import ConferencePage
from page_objects.screen_in_meeting import InMeetingPage
from page_objects.screen_leave_meeting import LeaveMeetingPage
from page_objects.screen_login import JoinMeetingPage


class TestConferenceSingleBrowser(object):
    # noinspection PyUnusedLocal,PyAttributeOutsideInit
    def setup_method(self, method):
        self.driver = webdriver.WebDriver(CHROMEDRIVER_PATH)
        self.driver.maximize_window()
        self.driver.get(CONF_URL)

        uid = uuid.uuid4()
        self.email_one = "{}{}{}".format('conf_emailOne', uid.hex[:7], "@test.com")
        self.name_one = "{}{}".format('conf_email', uid.hex[:7])

        self.login_page = JoinMeetingPage(self.driver)
        self.option_page = ConferencePage(self.driver)
        self.in_meeting_page = InMeetingPage(self.driver)
        self.leave_page = LeaveMeetingPage(self.driver)

    def test_check_valid_email(self):
        """Ensure an invalid email address cannot be used to join a
        Firefox user to the conference."""
        try:
            self.login_page.join_meeting(name=self.name_one, email="qwerty")
            assert self.login_page.email_validation_text()
        except AssertionError:
            save_screenshot(self.driver, self.__class__.__name__)
            raise

    def test_leave_conference(self):
        """both guests should logout and the login page should be visible again."""
        try:
            self.login_page.join_meeting(name=self.name_one, email=self.email_one)
            self.option_page.select_audio_option(audio=True)
            assert self.in_meeting_page.return_menu_audio_button_state().is_displayed()
            self.in_meeting_page.click_end_meeting_button()
            self.leave_page.click_leave_button()
            assert self.login_page.is_displayed_join_meeting()
        except AssertionError:
            save_screenshot(self.driver, self.__class__.__name__)
            raise

    # noinspection PyUnusedLocal,PyAttributeOutsideInit
    def teardown_method(self, method):
        self.driver.close()
        self.driver.quit()


class TestConferenceMultipleBrowser(object):
    # noinspection PyUnusedLocal,PyAttributeOutsideInit
    def setup_method(self, method):
        self.drivers = list()
        self.emails = list()
        self.names = list()
        for _ in range(2):
            driver = webdriver.WebDriver(CHROMEDRIVER_PATH)
            driver.maximize_window()
            driver.get(CONF_URL)
            self.drivers.append(driver)
            uid = uuid.uuid4()
            self.emails.append("{}{}{}".format('conf_emailOne', uid.hex[:7], "@test.com"))
            self.names.append("{}{}".format('conf_email', uid.hex[:7]))

        self.login_pages = [JoinMeetingPage(driver) for driver in self.drivers]
        self.option_pages = [ConferencePage(driver) for driver in self.drivers]
        self.in_meeting_pages = [InMeetingPage(driver) for driver in self.drivers]
        self.leave_pages = [LeaveMeetingPage(driver) for driver in self.drivers]

    def test_connect_two_users(self):
        """Join two different Firefox users to the conference. They should be permitted
        to join the conference with valid credentials.
        Ensure that the "Mute All" button does not appear in the left menu for any guest."""
        try:
            for login_page, option_page, in_meeting_page, name, email in zip(
                    self.login_pages, self.option_pages, self.in_meeting_pages, self.names, self.emails):
                login_page.join_meeting(name=name, email=email)
                option_page.select_audio_option(audio=True)
                assert in_meeting_page.return_menu_audio_button_state().is_displayed()
                assert "Mute All" not in in_meeting_page.driver.page_source
        except AssertionError:
            for driver in self.drivers:
                save_screenshot(driver, self.__class__.__name__)
            raise

    def test_chat_between_users(self):
        """Guest 1 should post a chat message "Ping". Guest 2 should
        see this message,and reply with "Pong"."""
        try:
            index = 0
            for login_page, option_page, in_meeting_page, name, email in zip(
                    self.login_pages, self.option_pages, self.in_meeting_pages, self.names,
                    self.emails):
                login_page.join_meeting(name=name, email=email)
                option_page.select_audio_option(audio=True)
                assert in_meeting_page.return_menu_audio_button_state().is_displayed()
                if index == 0:
                    message = "Ping"
                else:
                    message = "Pong"
                in_meeting_page.send_chat_message(message)
                index += 1
            assert "Ping", "Pong" in self.in_meeting_pages[-1].return_chat_text()
        except AssertionError:
            for driver in self.drivers:
                save_screenshot(driver, self.__class__.__name__)
            raise

    # noinspection PyUnusedLocal,PyAttributeOutsideInit
    # def teardown_method(self, method):
    #     for driver in self.drivers:
    #         driver.close()
    #         driver.quit()
