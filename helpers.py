import datetime

from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import element_to_be_clickable
from selenium.webdriver.support.wait import WebDriverWait

from conf_config import SCREENSHOT_PATH


def wait_for_element_to_be_clickable(self, find_element_by=By.ID, element_locator=None, timeout=20):
    WebDriverWait(self.driver, timeout).until(
        element_to_be_clickable((find_element_by, element_locator)))


def save_screenshot(driver, method):
    date = str(datetime.datetime.now())
    driver.save_screenshot(SCREENSHOT_PATH.format(method + date))
